﻿#include <iostream>
#include <cmath>
using namespace std;

void PrintNumber(int m, bool e)
{
    if (e == false)
    {
        cout << "Even numbers: ";
    }
    else
        cout << "Odd numbers: ";

    for (int x = e; x <= m; x += 2)
    {
        cout << x << " ";
    }
}

int main()
{
    int Limit;
    int num;
    std::cout << "Enter '0' to print even numbers or any other number to print odd numbers: ";
    std::cin >> num;
    std::cout << "Enter the maximum number: ";
    std::cin >> Limit;

    PrintNumber(Limit, num);
    return 0;
}
